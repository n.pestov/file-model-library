# FileModelLibrary

Библиотека для работы с файлами.

В данной библиотеке не реализована автоматическая синхронизация.

## Коцепции

Unit - единица структуры файла.

FileModel - модель файла, данные которой можно подвергнуть маршалингу.

BaseCollectionFileModel - базовая модель файла, в котором хранится коллекция единиц структуры файла.

## Пример работы

Пример создания и чтения файла AnalTask (SimpleFileModel):

            var analTask = new AnalTask();
            var fullPath = Path.GetTempFileName();
            var cancellationToken = CancellationToken.None;
            var model = new AnalTaskFileModel(fullPath);

            await model.CreateFileAsync(analTask, cancellationToken).ConfigureAwait(false);
            var actual = await model.ReadFileAsync(cancellationToken).ConfigureAwait(false);

Пример создания и чтения Pilot (CollectionFileModel):

            var pilotData = new PilotData();
            var recordFileNum = 1;
            for (uint i = 0; i < 10; ++i)
                pilotData.Add(new Pilot() { RecordFileNum = recordFileNum, BlockPosition = i });
            var fullPath = Path.GetTempFileName();
            var cancellationToken = CancellationToken.None;
            var model = new PilotFileModel(fullPath);

            await model.CreateFileAsync(pilotData, cancellationToken).ConfigureAwait(false);
            var actual = await model.ReadFileAsync(cancellationToken).ConfigureAwait(false);
