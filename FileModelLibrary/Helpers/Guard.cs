﻿using System;
using System.IO;

namespace Null.FileModelLibrary.Helpers
{
    internal static class Guard
    {
        internal static void ThrowIfNull<T>(T value, string parameterName)
        {
            if (value == null)
            {
                throw new ArgumentNullException(parameterName);
            }
        }

        internal static void ThrowIfFileSizeNotCorrect(long fileSize, int expectedFileSize)
        {
            if (!fileSize.Equals(expectedFileSize))
                throw new IOException("The file is incorrect. Its size does not match the necessary one.");
        }

        internal static void ThrowIfFileUnitSizeNotCorrect(int bytesCount, int expectedFileUnitSize)
        {
            if (!bytesCount.Equals(expectedFileUnitSize))
                throw new IOException("The file is incorrect. Its size does not match the necessary one.");
        }

        internal static void ThrowIfCollectionFileDataSizeNotCorrect(long fileSize, int unitSize)
        {
            if (fileSize % unitSize != 0)
                throw new IOException("The file is incorrect. Its size does not match the necessary one.");
        }
    }
}