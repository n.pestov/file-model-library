﻿using System.Runtime.InteropServices;

namespace Null.FileModelLibrary
{
    internal static class MarshalHelper
    {
        internal static byte[] GetBytes<T>(T obj)
        {
            var size = Marshal.SizeOf<T>();
            var ptr = Marshal.AllocHGlobal(size);

            var arr = new byte[size];

            try
            {
                Marshal.StructureToPtr(obj, ptr, true);
                Marshal.Copy(ptr, arr, 0, size);
                return arr;
            }
            finally
            {
                Marshal.FreeHGlobal(ptr);
            }
        }

        internal static T FromBytes<T>(byte[] array, int offset = 0) where T : new()
        {
            var size = Marshal.SizeOf<T>();
            var ptr = Marshal.AllocHGlobal(size);

            try
            {
                Marshal.Copy(array, offset, ptr, size);
                return Marshal.PtrToStructure<T>(ptr);
            }
            finally
            {
                Marshal.FreeHGlobal(ptr);
            }
        }
    }
}