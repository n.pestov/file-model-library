﻿using System.Threading;
using System.Threading.Tasks;

namespace Null.FileModelLibrary.FileModels.Collection.Abstract
{
    /// <summary>
    /// The interface is a basic model of a file that stores a collection of units.
    /// </summary>
    /// <typeparam name="TU">FileUnit</typeparam>
    public interface IBaseCollectionFileModel<TU>
    {
        /// <summary>
        /// Append units to file.
        /// </summary>
        /// <param name="unit">FileUnit</param>
        /// <param name="cancellationToken">CancellationToken</param>
        /// <returns></returns>
        Task AppendAsync(TU unit, CancellationToken cancellationToken);
    }
}