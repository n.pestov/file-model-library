﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Null.FileModelLibrary.FileModels.Simple.Abstract;
using Null.FileModelLibrary.Helpers;

namespace Null.FileModelLibrary.FileModels.Collection.Abstract
{
    /// <summary>
    /// The base model of a file that stores a collection of units.
    /// The model does not implement automatic synchronization.
    /// </summary>
    /// <typeparam name="T">FileData</typeparam>
    /// <typeparam name="TU">FileUnit</typeparam>
    public class BaseCollectionFileModel<T, TU> : BaseFileModel<T>, IBaseCollectionFileModel<TU>
        where T : ICollection<TU>, IEnumerable<TU>, new()
        where TU : new()
    {
        /// <summary>
        /// FileUnit size.
        /// </summary>
        protected static readonly int s_unitSize = Marshal.SizeOf<TU>();

        /// <summary>
        /// BaseCollectionFileModel ctor.
        /// </summary>
        /// <param name="fullPath">Path to file</param>
        public BaseCollectionFileModel(string fullPath) : base(fullPath) //FileShare?
        {
        }

        /// <inheritdoc/>
        public override async Task CreateFileAsync(T data, CancellationToken cancellationToken)
        {
            Guard.ThrowIfNull(data, nameof(data));

            using (var fileStream = new FileStream(_fullPath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite, s_unitSize, FileOptions.Asynchronous))
            {
                foreach (var unit in data)
                {
                    await fileStream.WriteAsync(MarshalHelper.GetBytes(unit).AsMemory(0, s_unitSize), cancellationToken).ConfigureAwait(false);
                }
            }
        }

        /// <inheritdoc/>
        public override async Task<T> ReadFileAsync(CancellationToken cancellationToken)
        {
            var data = new T();

            using (var fileStream = new FileStream(_fullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, s_unitSize, FileOptions.Asynchronous))
            {
                Guard.ThrowIfCollectionFileDataSizeNotCorrect(fileStream.Length, s_unitSize);

                var unitBytes = new byte[s_unitSize];

                while (fileStream.Position != fileStream.Length)
                {
                    var bytesCount = await fileStream.ReadAsync(unitBytes.AsMemory(0, s_unitSize), cancellationToken).ConfigureAwait(false);

                    Guard.ThrowIfFileUnitSizeNotCorrect(bytesCount, s_unitSize);

                    var unit = MarshalHelper.FromBytes<TU>(unitBytes);

                    data.Add(unit);
                }
            }

            return data;
        }

        /// <inheritdoc/>
        public override async Task UpdateFileAsync(T newData, CancellationToken cancellationToken)
        {
            Guard.ThrowIfNull(newData, nameof(newData));

            var previousState = await ReadFileAsync(cancellationToken).ConfigureAwait(false);

            FileStream? fileStream = null;

            try
            {
                fileStream = new FileStream(_fullPath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite, s_unitSize, FileOptions.Asynchronous);
                foreach (var unit in newData)
                    await fileStream.WriteAsync(MarshalHelper.GetBytes(unit).AsMemory(0, s_unitSize), cancellationToken).ConfigureAwait(false);
            }
            catch
            {
                if (Exists)
                {
                    await UndoChangesAsync(previousState, cancellationToken).ConfigureAwait(false);
                }

                throw;
            }
            finally
            {
                fileStream?.Dispose();
            }
        }

        /// <inheritdoc/>
        public async Task AppendAsync(TU unit, CancellationToken cancellationToken)
        {
            using (var fileStream = new FileStream(_fullPath, FileMode.Append, FileAccess.Write, FileShare.ReadWrite, s_unitSize, FileOptions.Asynchronous))
                await fileStream.WriteAsync(MarshalHelper.GetBytes(unit).AsMemory(0, s_unitSize), cancellationToken).ConfigureAwait(false);
        }
    }
}