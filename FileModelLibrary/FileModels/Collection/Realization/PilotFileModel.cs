﻿using Null.DataLibrary.CollectionData.Realization;
using Null.DataLibrary.Units;
using Null.FileModelLibrary.FileModels.Collection.Abstract;

namespace Null.FileModelLibrary.FileModels.Collection.Realization
{
    /// <summary>
    /// Pilot file model for storing pointers to HARD.
    /// </summary>
    public sealed class PilotFileModel : BaseCollectionFileModel<PilotData, Pilot>
    {
        /// <summary>
        /// PilotFileModel ctor.
        /// </summary>
        /// <param name="fullPath">Path to file</param>
        public PilotFileModel(string fullPath) : base(fullPath)
        {
        }
    }
}