﻿using Null.DataLibrary.CollectionData.Realization;
using Null.DataLibrary.Units;
using Null.FileModelLibrary.FileModels.Collection.Abstract;

namespace Null.FileModelLibrary.FileModels.Collection.Realization
{
    /// <summary>
    /// Event file model for patient events CRUD.
    /// </summary>
    public sealed class EventFileModel : BaseCollectionFileModel<EventData, Event>
    {
        /// <summary>
        /// EventFileModel ctor.
        /// </summary>
        /// <param name="fullPath">Path to file</param>
        public EventFileModel(string fullPath) : base(fullPath)
        {
        }
    }
}