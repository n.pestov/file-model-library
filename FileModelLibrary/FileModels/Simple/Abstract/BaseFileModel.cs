﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Null.FileModelLibrary.Helpers;

namespace Null.FileModelLibrary.FileModels.Simple.Abstract
{
    /// <summary>
    /// The base model of the file.
    /// </summary>
    /// <typeparam name="T">FileUnit</typeparam>
    public abstract class BaseFileModel<T> : IBaseFileModel<T>, IUndoState<T>
    {
        /// <summary>
        /// Full path to file.
        /// </summary>
        protected readonly string _fullPath;

        /// <summary>
        /// Check file exists.
        /// </summary>
        public bool Exists => File.Exists(_fullPath);

        /// <summary>
        /// BaseFileModel ctor.
        /// </summary>
        /// <param name="fullPath">Path to file</param>
        public BaseFileModel(string fullPath)
        {
            Guard.ThrowIfNull(fullPath, nameof(fullPath));

            _fullPath = fullPath;
        }

        /// <inheritdoc/>
        public abstract Task CreateFileAsync(T data, CancellationToken cancellationToken);

        /// <inheritdoc/>
        public abstract Task<T> ReadFileAsync(CancellationToken cancellationToken);

        /// <inheritdoc/>
        public abstract Task UpdateFileAsync(T newData, CancellationToken cancellationToken);

        /// <inheritdoc/>
        public void DeleteFile() => File.Delete(_fullPath);

        /// <inheritdoc/>
        public async Task UndoChangesAsync(T previousState, CancellationToken cancellationToken)
        {
            DeleteFile();
            await CreateFileAsync(previousState, cancellationToken).ConfigureAwait(false);
        }
    }
}