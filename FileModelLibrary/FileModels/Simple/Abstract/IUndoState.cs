﻿using System.Threading;
using System.Threading.Tasks;

namespace Null.FileModelLibrary.FileModels.Simple.Abstract
{
    /// <summary>
    /// Interface for undo the state of an object.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal interface IUndoState<T>
    {
        /// <summary>
        /// Undo the state.
        /// </summary>
        /// <param name="previousState"></param>
        /// <param name="cancellationToken">CancellationToken</param>
        /// <returns></returns>
        Task UndoChangesAsync(T previousState, CancellationToken cancellationToken);
    }
}