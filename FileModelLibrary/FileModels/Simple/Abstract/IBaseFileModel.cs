﻿using System.Threading;
using System.Threading.Tasks;

namespace Null.FileModelLibrary.FileModels.Simple.Abstract
{
    /// <summary>
    /// Interface of the base file model.
    /// </summary>
    /// <typeparam name="T">FileUnit</typeparam>
    public interface IBaseFileModel<T>
    {
        /// <summary>
        /// Create file.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="cancellationToken">CancellationToken</param>
        /// <returns></returns>
        Task CreateFileAsync(T data, CancellationToken cancellationToken);
        /// <summary>
        /// Read file.
        /// </summary>
        /// <param name="cancellationToken">CancellationToken</param>
        /// <returns></returns>
        Task<T> ReadFileAsync(CancellationToken cancellationToken);
        /// <summary>
        /// Update file.
        /// </summary>
        /// <param name="newData"></param>
        /// <param name="cancellationToken">CancellationToken</param>
        /// <returns></returns>
        Task UpdateFileAsync(T newData, CancellationToken cancellationToken);

        /// <summary>
        /// Delete file.
        /// </summary>
        void DeleteFile();
    }
}