﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Null.FileModelLibrary.Helpers;

namespace Null.FileModelLibrary.FileModels.Simple.Abstract
{
    /// <summary>
    /// A file model with the possibility of simple marshaling of file data.
    /// The model does not implement automatic synchronization.
    /// </summary>
    /// <typeparam name="T">FileUnit</typeparam>
    public class SimpleFileModel<T> : BaseFileModel<T>
        where T : new()
    {
        /// <summary>
        /// FileUnit size.
        /// </summary>
        protected static readonly int s_bytesCount = Marshal.SizeOf<T>();

        /// <summary>
        /// File size.
        /// </summary>
        public int Length => s_bytesCount;

        /// <summary>
        /// SimpleFileModel ctor.
        /// </summary>
        /// <param name="fullPath">Path to file</param>
        public SimpleFileModel(string fullPath) : base(fullPath)
        {
        }

        /// <inheritdoc/>
        public override async Task CreateFileAsync(T data, CancellationToken cancellationToken)
        {
            Guard.ThrowIfNull(data, nameof(data));

            using (var fileStream = new FileStream(_fullPath, FileMode.Create, FileAccess.Write, FileShare.Read, s_bytesCount, FileOptions.Asynchronous))
                await fileStream.WriteAsync(MarshalHelper.GetBytes(data).AsMemory(0, s_bytesCount), cancellationToken).ConfigureAwait(false);
        }

        /// <inheritdoc/>
        public override async Task<T> ReadFileAsync(CancellationToken cancellationToken)
        {
            var res = new byte[s_bytesCount];

            using (var fileStream = new FileStream(_fullPath, FileMode.Open, FileAccess.Read, FileShare.Read, s_bytesCount, FileOptions.Asynchronous))
            {
                var bytesCount = await fileStream.ReadAsync(res.AsMemory(0, s_bytesCount), cancellationToken).ConfigureAwait(false);

                Guard.ThrowIfFileSizeNotCorrect(fileStream.Length, s_bytesCount);
            }

            return MarshalHelper.FromBytes<T>(res);
        }

        /// <inheritdoc/>
        public override async Task UpdateFileAsync(T newData, CancellationToken cancellationToken)
        {
            Guard.ThrowIfNull(newData, nameof(newData));

            var previousState = await ReadFileAsync(cancellationToken).ConfigureAwait(false);

            FileStream? fileStream = null;

            try
            {
                fileStream = new FileStream(_fullPath, FileMode.Create, FileAccess.Write, FileShare.Read, s_bytesCount, FileOptions.Asynchronous);
                await fileStream.WriteAsync(MarshalHelper.GetBytes(newData).AsMemory(0, s_bytesCount), cancellationToken).ConfigureAwait(false);
            }
            catch
            {
                if (Exists)
                {
                    await UndoChangesAsync(previousState, cancellationToken).ConfigureAwait(false);
                }

                throw;
            }
            finally
            {
                fileStream?.Dispose();
            }
        }
    }
}